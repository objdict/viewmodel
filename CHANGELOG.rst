=====================
Change News & Updates
=====================


Version 0.3.5 - 31 May 2018
===========================
Fix for a failing test and some general clean-up of the tests

Version 0.3.5 - 29 May 2018
===========================
Updated the README file to include instructions on how to use JSONLoad and to remove typos and grammatical errors.

Other updates include:
    * added CHANGELOG.rst
    * added CONTRIBUTORS.rst
    * renamed a strangely named test filename
    * some reformatting

Version 0.3.4
===========================
