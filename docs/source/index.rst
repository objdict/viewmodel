.. ViewMode documentation master file, created by copying from another project instead of by sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least contain the root `toctree` directive.

ViewModel documentation
========================

Contents:

.. toctree::
    :glob:
    :maxdepth: 4
    :numbered:

    readmelnk
    code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
