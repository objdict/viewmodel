from decimal import Decimal, ROUND_HALF_UP

import pytest
from bson import Decimal128
from objdict import ObjDict, dumps
from viewmodel import BaseView
from viewmodel.viewFields import ObjListField, DecField, SetField, BoolField, viewModelDB

testFields = viewModelDB.baseDB.db.testFields


class TestSetField:
	class SetView(BaseView):
		models_ = testFields
		field = SetField()

		def getRows_(self, query=None):
			if query is None:
				return []

			return self.models_.find(query)

	the_list = [1, 1, 2, 3]  # we want duplicates for the sake of the tests
	the_set = [1, 2, 3]  # not actually a set, but no duplicates

	def test_default(self):
		assert SetField().default() == list()

	def test_type(self):
		assert SetField().Type() is list

	def test_insert_list_removes_duplicates_for_database(self):
		with self.SetView().insert_() as view:
			view.field = self.the_list

		data = self.SetView({"field": self.the_set})
		assert data.field == self.the_set

	def test_set_unsupported_type_gives_error(self):
		"""
		No point in the set field allowing lists to be saved to the database, or any other type
		because then we might as well just use a list field.
		"""
		with pytest.raises(AssertionError):
			with self.SetView().insert_() as view:
				view.field = "I am not a list or a set"

	def test_data_is_list_in_json(self):
		"""
		JSON does not support sets so the SetField must convert to a list
		when converting to JSON format.
		"""
		view = self.SetView({})
		data = ObjDict.dumps(view)
		json = ObjDict.loads(data)
		assert json.data[0].field == ["testSetField"]


class TestDecField:
	class DecimalView(BaseView):
		models_ = testFields
		decField = DecField("0.01")

	def test_dec_field_read_write(self):
		testDecimal = Decimal("270")
		with self.DecimalView().insert_() as row:
			row.decField = testDecimal

		data = self.DecimalView({"decField": Decimal128(testDecimal)})
		assert data.decField == testDecimal

	def test_dec_field_2dp_quantisation(self):
		testDecimal = Decimal("0.00")
		with self.DecimalView().insert_() as row:
			row.decField = testDecimal

		data = self.DecimalView({"decField": Decimal128(testDecimal)})
		assert (data.decField) == (testDecimal)

	def test_dumps_to_json_as_string(self):
		testDecimal = Decimal("99")
		with self.DecimalView().insert_() as row:
			row.decField = testDecimal

		returnedDecimal = Decimal("99.00")
		view = self.DecimalView({})
		json_string = dumps(view)
		assert f'"decField": "{returnedDecimal}"' in json_string


class TestBoolField:
	class BoolView(BaseView):
		models_ = testFields
		verified = BoolField()

	def test_bool_field_default_value(self):
		view = self.BoolView()
		default = view.verified
		assert default is False

	def test_bool_field_read_write(self):
		testBool = True
		with self.BoolView().insert_() as row:
			row.verified = testBool

		data = self.BoolView({"verified": testBool})
		assert data.verified == testBool