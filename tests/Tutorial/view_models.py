from datetime import datetime


from objdict import Struct

from viewmodel import ObjectId

from viewmodel import JournalView
from viewmodel import ViewRow
from viewmodel import IdField
from viewmodel import IntField
from viewmodel import TxtField
from viewmodel import BaseView
from viewmodel import DBMongoSource

from viewmodel.viewFields import viewModelDB

database = viewModelDB.baseDB.db


class Student(BaseView):
    models_ = database.Students
    id = IdField(cases={})  # , name = '_id')
    name = TxtField()
    course = TxtField(value='engineering')
    course_year = IntField()


class StudentWithNameFieldWithAlias(BaseView):
    models_ = database.Students
    id = IdField(cases={})  # , name = '_id')
    first_name = TxtField(src='.name')  # alias used for 'name' in collection
    course = TxtField(value='engineering')
    course_year = IntField()


class Courses(BaseView):
    models_ = database.Courses
    id = IdField(cases={})  # , name = '_id')
    name = TxtField()
    course = TxtField(value='engineering')
    course_year = IntField()

