from decimal import Decimal, ROUND_HALF_UP
from typing import Union

import pytest
from bson import Decimal128
from objdict import ObjDict, dumps
from viewmodel import BaseView
from dataclasses import dataclass, asdict
from viewmodel.viewFields import ObjListField, DecField, SetField, BoolField, TxtField, viewModelDB

testFields = viewModelDB.baseDB.db.testObjListFields


@dataclass
class Number:
	number: str 
	extra: str = "not yet used"


class SampleLists(BaseView):
	models_ = None
	obj_list = ObjListField("obj list")


class BaseView(BaseView):
	models_ = testFields
	nested = ObjListField(elementObjType=Number)
	names = TxtField("Name Field")
	

@pytest.fixture
def ObjListFieldView():
	return BaseView({})


@pytest.fixture
def AndrewView():
	view = BaseView({"names": "Andrew"})
	if len(view) == 0:
		row = view.insert_()
	else:
		row = view[0]
	with row:
		row.names = "Andrew"
		row.nested = [Number("one"), Number("two"), Number("three")]
	return BaseView({"names": "Andrew"})


class TestIdxKeys:

	def test_idxKey(self):
		idxKey = ObjListField.idxKey
		assert idxKey(5) == "0005", "idxkey should work with int"
		assert idxKey("5") == "0005", "idxkey should work with str"
		assert idxKey("5.2") == "0005.0002", "idxkey should allow 'dots'"

	def test_idxKeyVal(self):
		idxKeyVal = ObjListField.idxKeyVal
		assert idxKeyVal(5) == 5, "idxkeyval should allow int"
		assert idxKeyVal("5") == 5, "idxkeyval should allow str"
		assert idxKeyVal("5.2") == 6, "idxkeyval return indictates beyond base int"


class TestObjListField:
	expectedData = [Number("one"), Number("two"), Number("three")]

	def test_txtField_write(self, ObjListFieldView):
		assert len(ObjListFieldView) == 0
		
		with ObjListFieldView.insert_() as row:
			row.names = "Peter"
		
		assert len(ObjListFieldView) == 1
		assert ObjListFieldView.names == "Peter"
		
	def test_objlist_write_with_map(self, ObjListFieldView):
		expected = [{"number":"one"}, {"number":"two"}, {"number":"three"}]
		row = ObjListFieldView.insert_() 
		with row:
			row.names = "Geoff"
			row.nested = expected
		
		assert row.nested == self.expectedData

	def test_objlist_write_with_object(self, ObjListFieldView):
		expected = [Number("four"), Number("two"), Number("three")]

		row = ObjListFieldView.insert_() 
		with row:
			row.names = "Andrew"
			row.nested = expected
		
		assert row.nested == expected
	
	def test_objlist_object_field_update(self, AndrewView):
		expected = [Number("one"), Number("seven"), Number("three"), Number("eighteen")]
		updates = {"001": {"number": "seven"}, "003": {"number": "eighteen"}}

		row = AndrewView[0]  # should be andrew
		with row:
			row.nested = updates
		
		assert row.nested == expected
	
	def test_db_saved_values(self, ObjListFieldView):
		assert ObjListFieldView[0].names == "Peter"
		assert ObjListFieldView[1].names == "Geoff"
		assert ObjListFieldView[2].names == "Andrew"

	def test_objlist_object_update_field_with_deletion(self, AndrewView):
		expected = [Number("one"), Number("seven"), Number("eighteen")]
		# updates = {"001": None}
		updates = {"001": None, "002": {"number": "seven"}, "003": {"number": "eighteen"}}

		row = AndrewView[0]  # should be andrew
		with row:
			row.nested = updates
		
		assert row.nested == expected

	def test_obj_list_set_str(self):
		""" set from str only valid for json str"""
		sample_list = SampleLists()

		sample_list.obj_list = "test"  # cannot set as non json string so get empty list
		assert sample_list.obj_list == []
		sample_list.obj_list = '[{"a":1}]'  # valid json
		assert sample_list.obj_list[0]['a'] == 1

	def xtest_obj_list_with_escape_in_string(self):
		obj_list = SampleLists()
		obj_list.obj_list = '[{"\"under\"the weather"}]'
		assert obj_list.obj_list == '[{"\"under\"the weather"}]'